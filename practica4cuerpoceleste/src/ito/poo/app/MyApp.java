package ito.poo.app;
import ito.poo.clases.CuerpoCeleste;
import ito.poo.clases.Ubicacion;

public class MyApp {

	static void run() {
		
		CuerpoCeleste CuCe = new CuerpoCeleste("Luna", "Espacio", "Queso");
		
		System.out.println(CuCe);
		
		Ubicacion ubi = new Ubicacion(2000f, 25900f, "Estrella" , 660f);
		
		System.out.println(ubi);
	}
	
	public static void main(String[] args) {
		run();
		// TODO Auto-generated method stub

	}

}
